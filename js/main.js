/*global $ */
/*eslint-env browser*/

//========================Load=====================//
$(document).ready(function () {
    "use strict";
    $(window).on('load',function () {
        $(".load").fadeOut(100, function () {
            $("body").css("overflow", "auto");
            $(this).fadeOut(100, function () {
                $(this).remove();
            });
        });
    });

});
//=======================Slider=====================//
/* slider at mobile in home page */
$(document).ready(function (){
    'use strict';
    
    $(".about .slider").slick({
        rtl: true,
        nextArrow:"",
        prevArrow:"",
        dots:true,
        infinite:true,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow:3,
        slidesToScroll:3,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    function reslider(){
        if ($('.container').width() <= 520 ){
            $(".service .slider").slick({
                rtl: true,
                nextArrow:"",
                prevArrow:"",
                dots:true,
                infinite:false,
                autoplay: true,
                autoplaySpeed: 1500,
                slidesToShow:2,
                slidesToScroll:2,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                        }
                    }
                ]
            });
        }
        $(".sponsor .slider").slick({
            rtl: true,
            nextArrow:"",
            prevArrow:"",
            dots:false,
            autoplay: true,
            autoplaySpeed: 1500,
            slidesToShow:6,
            slidesToScroll:6,
            
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        centerMode:true,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        
                    }
                }
            ]
        });
       
    };
    reslider();
    $(window).resize(function(){     
        reslider();
    }); 
}); 

//====================== Sync Load ======================//
$(document).ready(function () {
    'use strict';
    $(window).click(function() {
        $(".collapse").removeClass("show");
    });
    
    $('.collapse ').click(function(event){
//        event.stopPropagation();
    });
     $("nav li a, .home a").click(function (){

//           e.preventDefault();
            $("html, body").animate({
                scrollTop: $("#"+ $(this).data("scroll")).offset().top
            }, 1000);
          //Add Class Active
//          $(this).addClass("active").parent().siblings().find("a").removeClass("active");
       });
        // Sycn Navbar
  
        $(window).scroll(function (){
            $("section").each(function (){
                if($(window).scrollTop() > ($(this).offset().top - 50)){
                    var sectionID=$(this).attr("id");
                    $("nav li a ").removeClass("active");
                    $("nav li a[data-scroll='"+ sectionID +"']").addClass("active");
                }
            })
        })
});


//========================Scroll To Top=====================//
$(document).ready(function (){

  var scrollButton=$("#scroll");

  $(window).scroll(function(){
      if($(this).scrollTop()>=700)
      {
          scrollButton.show();
      }
      else
      {
          scrollButton.hide();
      }
  });
  scrollButton.click(function(){
      $("html,body").animate({scrollTop:0},900);
  });
});

//======================== Sweetalert =======================//
$(document).ready(function (){
   'use strict';
    $('.send').click(function (){
        swal({
            title: "تم التواصل بنجاح!",
            icon: "success",
            button: "تم",
        });
    });

});

//======================== Counter ========================//
//(function ($) {
//	$.fn.countTo = function (options) {
//		options = options || {};
//		
//		return $(this).each(function () {
//			// set options for current element
//			var settings = $.extend({}, $.fn.countTo.defaults, {
//				from:            $(this).data('from'),
//				to:              $(this).data('to'),
//				speed:           $(this).data('speed'),
//				refreshInterval: $(this).data('refresh-interval'),
//				decimals:        $(this).data('decimals')
//			}, options);
//			
//			// how many times to update the value, and how much to increment the value on each update
//			var loops = Math.ceil(settings.speed / settings.refreshInterval),
//				increment = (settings.to - settings.from) / loops;
//			
//			// references & variables that will change with each update
//			var self = this,
//				$self = $(this),
//				loopCount = 0,
//				value = settings.from,
//				data = $self.data('countTo') || {};
//			
//			$self.data('countTo', data);
//			
//			// if an existing interval can be found, clear it first
//			if (data.interval) {
//				clearInterval(data.interval);
//			}
//			data.interval = setInterval(updateTimer, settings.refreshInterval);
//			
//			// initialize the element with the starting value
//			render(value);
//			
//			function updateTimer() {
//				value += increment;
//				loopCount++;
//				
//				render(value);
//				
//				if (typeof(settings.onUpdate) == 'function') {
//					settings.onUpdate.call(self, value);
//				}
//				
//				if (loopCount >= loops) {
//					// remove the interval
//					$self.removeData('countTo');
//					clearInterval(data.interval);
//					value = settings.to;
//					
//					if (typeof(settings.onComplete) == 'function') {
//						settings.onComplete.call(self, value);
//					}
//				}
//			}
//			
//			function render(value) {
//				var formattedValue = settings.formatter.call(self, value, settings);
//				$self.html(formattedValue);
//			}
//		});
//	};
//	
//	$.fn.countTo.defaults = {
//		from: 0,               // the number the element should start at
//		to: 0,                 // the number the element should end at
//		speed: 1000,           // how long it should take to count between the target numbers
//		refreshInterval: 100,  // how often the element should be updated
//		decimals: 0,           // the number of decimal places to show
//		formatter: formatter,  // handler for formatting the value before rendering
//		onUpdate: null,        // callback method for every time the element is updated
//		onComplete: null       // callback method for when the element finishes updating
//	};
//	
//	function formatter(value, settings) {
//		return value.toFixed(settings.decimals);
//	}
//}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
//========================WOW=====================//
// Init WOW.js and get instance
var wow = new WOW();
wow.init();